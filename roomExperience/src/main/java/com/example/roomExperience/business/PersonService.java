package com.example.roomExperience.business;

import com.example.roomExperience.apiModel.Person;
import com.example.roomExperience.connection.PersonApi;
import java.io.IOException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PersonService implements InterfaceService<Person, Long> {

  @Autowired
  PersonApi api;

  @Autowired
  BCryptPasswordEncoder passwordEncoder;

  @Override
  public Person insert(Person person) throws IOException {
      person.setPassword(passwordEncoder.encode(person.getPassword()));
      return api.insert(person).execute().body();
  }

  @Override
  public Optional<Person> findById(Long id) throws IOException {
      return Optional.ofNullable(api.findById(id).execute().body());
  }

  public Optional<Person> findByUserName(String userName) throws IOException {
    return Optional.ofNullable(api.findByUserName(userName).execute().body());
  }

  @Override
  public Iterable<Person> findAll() throws IOException {
      return api.get().execute().body();
  }

  @Override
  public Person update(Person person) throws IOException {
      return api.update(person.getId(), person).execute().body();
  }

  @Override
  public void delete(Long personId) throws Exception {
      api.delete(findById(personId)
          .orElseThrow(
              () -> new Exception("Not found person")).getId());
  }

}
