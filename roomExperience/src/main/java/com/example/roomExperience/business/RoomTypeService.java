package com.example.roomExperience.business;

import com.example.roomExperience.apiModel.RoomType;
import com.example.roomExperience.connection.RoomTypeApi;
import java.io.IOException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomTypeService implements InterfaceService<RoomType, Long> {

  @Autowired
  RoomTypeApi api;

  @Override
  public RoomType insert(RoomType roomType) throws IOException {
      return api.insert(roomType).execute().body();
  }

  @Override
  public Optional<RoomType> findById(Long id) throws IOException {
      return Optional.ofNullable(api.findById(id).execute().body());
  }

  @Override
  public Iterable<RoomType> findAll() throws IOException {
      return api.get().execute().body();
  }

  @Override
  public RoomType update(RoomType roomType) throws IOException {
      return api.update(roomType.getId(), roomType).execute().body();
  }

  @Override
  public void delete(Long roomTypeId) throws Exception {
      api.delete(findById(roomTypeId)
          .orElseThrow(() -> new Exception("Not found roomType")).getId());
  }


}
