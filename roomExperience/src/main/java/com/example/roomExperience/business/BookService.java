package com.example.roomExperience.business;

import com.example.roomExperience.apiModel.Book;
import com.example.roomExperience.apiModel.Person;
import com.example.roomExperience.apiModel.Room;
import com.example.roomExperience.apiModel.RoomStatus;
import com.example.roomExperience.connection.BookApi;
import java.io.IOException;
import java.util.Date;
import java.util.Optional;
import javax.management.InstanceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BookService implements InterfaceService<Book, Long> {

  @Autowired
  BookApi api;

  @Autowired
  PersonService personService;

  @Autowired
  RoomService roomService;

  @Autowired
  RoomStatusService roomStatusService;

  public Book booking(long idPerson, long idRoom, Date checkIn, Date checkOut)
      throws Exception {
    Room room = roomService.findById(idRoom).orElseThrow(
        InstanceNotFoundException::new);
    Person person = personService.findById(idPerson).orElseThrow(
        InstanceNotFoundException::new);
    if (room.getRoomStatus().getStatus() == RoomStatus.BOOK_FREE.getStatus()) {
      Book book = new Book();
      book.setRoom(roomService.changeRules(room,
          roomStatusService.findByName(RoomStatus.BOOK_BUSY.getStatusName())));
      roomService.update(book.getRoom());
      book.setReserve(new Date());
      book.setCheckIn(checkIn);
      book.setCheckOut(checkOut);
      book.setPerson(person);
      book.setRoom(room);
      return api.insert(book).execute().body();
    } else {
      throw new InstanceNotFoundException();
    }
  }

  @Override
  public Book insert(Book book) throws IOException {
    return api.insert(book).execute().body();
  }

  @Override
  public Optional<Book> findById(Long id) throws IOException {
    return Optional.ofNullable(api.findById(id).execute().body());
  }

  @Override
  public Iterable<Book> findAll() throws IOException {
    return api.get().execute().body();
  }

  @Override
  public Book update(Book book) throws IOException {
    return api.update(book.getId(), book).execute().body();
  }

  @Override
  public void delete(Long bookId) throws Exception {
    api.delete(findById(bookId)
        .orElseThrow(() -> new Exception("Not found book")).getId());
  }

}
