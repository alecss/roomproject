package com.example.roomExperience.business;

import com.example.roomExperience.apiModel.Room;
import com.example.roomExperience.apiModel.RoomStatus;
import com.example.roomExperience.apiModel.RoomType;
import com.example.roomExperience.connection.RoomApi;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Supplier;
import javax.management.InstanceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomService implements InterfaceService<Room, Long> {

  public final static RoomType ROOM_TYPE_ENUM_NORMAL = new RoomType("NORMAL", 3);
  public final static RoomType ROOM_TYPE_ENUM_STANDARD = new RoomType("STANDARD", 6);
  public final static RoomType ROOM_TYPE_ENUM_SUITE = new RoomType("SUITE", 9);

  @Autowired
  RoomApi api;

  @Autowired
  RoomStatusService roomStatusService;

  @Override
  public Room insert(Room room) throws Exception {
    if (!Optional.ofNullable(api.findByName(room.getRoomName()).execute().body())
    .isPresent()) {
      return api.insert(changeRules(room,
          roomStatusService.findByName(room.getRoomStatus().getStatusName()))).execute().body();
    } else {
      throw new Exception("Room exists");
    }

  }

  @Override
  public Optional<Room> findById(Long id) throws IOException {
    return Optional.ofNullable(api.findById(id).execute().body());
  }

  public Optional<Room> findByFree(Long id) throws IOException {
    return Optional.ofNullable(api.findByFreeRoomId(id).execute().body());
  }

  @Override
  public Iterable<Room> findAll() throws IOException {
    return api.get().execute().body();
  }

  public Iterable<Room> findAllFree() throws IOException {
      return api.findByFree().execute().body();
  }

  public Iterable<Room> findRoomsAndType(String roomType) throws IOException {
    return api.findRoomsAndType(roomType).execute().body();
  }

  public Iterable<Room> findRoomsAndStatus(String status) throws IOException{
    return api.findRoomsAndStatus(status).execute().body();
  }

  @Override
  public Room update(Room room) throws Exception {
    return api.update(room.getId(), changeRules(room,
        roomStatusService.findByName(room.getRoomStatus().getStatusName())))
        .execute().body();
  }

  @Override
  public void delete(Long roomId) throws Exception {
    api.delete(findById(roomId)
        .orElseThrow(
            () -> new Exception("Not found room")).getId());
  }

  public Optional<Room> changeStatus(long idRoom, long idRoomStatus) throws Exception {
    return Optional.of(insert(
        changeRules(
            findById(idRoom)
                .orElseThrow(InstanceNotFoundException::new),
            roomStatusService.findById(idRoomStatus)
                .orElseThrow(InstanceNotFoundException::new)
        )));
  }

  public Room changeRules(Room room, RoomStatus roomStatus) throws Exception {
    if (room.getRoomStatus().getStatus() == roomStatus.getStatus()) {
      return room;
    }
    switch (room.getRoomStatus().getStatusName()) {
      case "FREE":
        if (roomStatus.getStatus() == RoomStatus.BOOK_MAINTENANCE.getStatus()
            || roomStatus.getStatus() == RoomStatus.BOOK_BUSY.getStatus()) {
            room.setRoomStatus(roomStatus);
          return room;
        } else {
          throw new Exception("Error change status");
        }
      case "BUSY":
        if (roomStatus.getStatus() == RoomStatus.BOOK_MAINTENANCE.getStatus()
            || roomStatus.getStatus() == RoomStatus.BOOK_CLEANING.getStatus()) {
          room.setRoomStatus(roomStatus);
          return room;
        } else {
          throw new Exception("Error change status");
        }
      case "CLEANING":
      case "MAINTENANCE":
        if (roomStatus.getStatus() == RoomStatus.BOOK_FREE.getStatus()
            || roomStatus.getStatus() == RoomStatus.BOOK_CLEANING.getStatus()) {
          room.setRoomStatus(roomStatus);
          return room;
        } else {
          throw new Exception("Error change status: not valid");
        }
      default:
        throw new Exception("Error change status: not found");
    }
  }


}
