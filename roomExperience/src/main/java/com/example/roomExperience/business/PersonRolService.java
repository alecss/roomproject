package com.example.roomExperience.business;

import com.example.roomExperience.apiModel.PersonRol;
import com.example.roomExperience.connection.PersonRolApi;
import java.io.IOException;
import java.util.Optional;
import java.util.function.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonRolService implements InterfaceService<PersonRol, Long> {

  @Autowired
  PersonRolApi api;

  @Override
  public PersonRol insert(PersonRol personRol) throws IOException {
      return api.insert(personRol).execute().body();
  }

  @Override
  public Optional<PersonRol> findById(Long id) throws IOException {
      return Optional.ofNullable(api.findById(id).execute().body());
  }

  @Override
  public Iterable<PersonRol> findAll() throws IOException {
      return api.get().execute().body();
  }

  @Override
  public PersonRol update(PersonRol personRol) throws IOException {
      return api.update(personRol.getId(), personRol).execute().body();
  }

  @Override
  public void delete(Long personRolId) throws Exception {
      api.delete(findById(personRolId)
          .orElseThrow(() -> new Exception("Not found personRol")).getId());
  }


}
