package com.example.roomExperience.business;

import com.example.roomExperience.apiModel.RoomStatus;
import com.example.roomExperience.connection.RoomStatusApi;
import java.io.IOException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoomStatusService implements InterfaceService<RoomStatus, Long> {

  @Autowired
  RoomStatusApi api;

  @Override
  public RoomStatus insert(RoomStatus book) throws IOException {
      return api.insert(book).execute().body();
  }

  @Override
  public Optional<RoomStatus> findById(Long id) throws IOException {
      return Optional.ofNullable(api.findById(id).execute().body());
  }

  @Override
  public Iterable<RoomStatus> findAll() throws IOException {
      return api.get().execute().body();
  }

  @Override
  public RoomStatus update(RoomStatus book) throws IOException {
      return api.update(book.getId(), book).execute().body();
  }

  @Override
  public void delete(Long bookId) throws Exception {
      api.delete(findById(bookId)
          .orElseThrow(() -> new Exception("Not found book")).getId());
  }


  public RoomStatus findByName(String statusName) throws IOException {
      return Optional.ofNullable(api.findByName(statusName).execute().body()).get();
  }
}
