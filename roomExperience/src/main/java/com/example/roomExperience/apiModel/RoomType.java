package com.example.roomExperience.apiModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class RoomType {

  public RoomType(String roomType, int capacity){
    this.capacity = capacity;
    this.roomType = roomType;
  }

  private long id;
  private String roomType;
  private int capacity;

}