package com.example.roomExperience.apiModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class RoomStatus {

  private static final String FREE = "FREE";
  private static final String BUSY = "BUSY";
  private static final String RESERVED = "RESERVED";
  private static final String MAINTENANCE = "MAINTENANCE";
  private static final String CLEANING = "CLEANING";

  public static final RoomStatus BOOK_FREE = new RoomStatus(FREE, 0);
  public static final RoomStatus BOOK_RESERVED = new RoomStatus(RESERVED, 4);
  public static final RoomStatus BOOK_BUSY = new RoomStatus(BUSY, 1);
  public static final RoomStatus BOOK_MAINTENANCE = new RoomStatus(MAINTENANCE, 2);
  public static final RoomStatus BOOK_CLEANING  = new RoomStatus(CLEANING , 3);

  private long id;
  private String statusName;
  private int status;

  public RoomStatus(String s, int i) {
    this.status = i;
    this.statusName = s;
  }
}