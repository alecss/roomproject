package com.example.roomExperience.apiModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PersonRol {

  public final static PersonRol ROLE_CLIENT = new PersonRol("CLIENT");
  public final static PersonRol ROLE_MANAGER = new PersonRol("MANAGER");
  public final static PersonRol ROLE_RECEPTIONIST = new PersonRol("RECEPTIONIST");

  public PersonRol(String rolName) {
    this.rolName = rolName;
  }

  private long id;
  private String rolName;

}
