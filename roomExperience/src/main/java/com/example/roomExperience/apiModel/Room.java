package com.example.roomExperience.apiModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class Room {

  private long id;
  private String roomName;
  private int roomNumber;

  private RoomType roomType;
  private RoomStatus roomStatus;

}
