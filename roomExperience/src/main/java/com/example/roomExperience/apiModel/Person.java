package com.example.roomExperience.apiModel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Person {

  private long id;

  private String userName;
  private String firstName;
  private String lastName;
  private boolean enabled;
  private String password;
  private PersonRol personRol;

}
