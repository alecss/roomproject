package com.example.roomExperience.apiModel;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Book {

  private long id;

  private Person person;
  private Room room;

  @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss", shape = Shape.STRING)
  private Date reserve;

  @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss", shape = Shape.STRING)
  private Date checkIn;

  @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss", shape = Shape.STRING)
  private Date checkOut;
}
