package com.example.roomExperience;

import com.example.roomExperience.connection.PersonApi;
import java.io.IOException;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RoomExperienceApplication {

	@Autowired
	PersonApi api;

	@PostConstruct
	void init(){
		try {
			System.out.println(api.get().execute().body());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(RoomExperienceApplication.class, args);
	}

}
