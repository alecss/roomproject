package com.example.roomExperience.security.service;

import com.example.roomExperience.business.PersonService;
import com.example.roomExperience.security.model.UserDetailsCustom;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Slf4j
public class UserDetailsCustomService implements UserDetailsService {

  @Autowired
  private PersonService personService;

  @Override
  public UserDetails loadUserByUsername(String username)
      throws UsernameNotFoundException {
    try {
      log.info(":::::userName {} -> ", username);
      return new UserDetailsCustom(personService.findByUserName(username)
          .orElseThrow(() -> new UsernameNotFoundException("Could not find user")));
    } catch (IOException ioException) {
      ioException.printStackTrace();
      log.info("bad request::::: ");
      return null;
    }
  }
}