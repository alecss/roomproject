package com.example.roomExperience.security.model;

import com.example.roomExperience.apiModel.Person;
import com.example.roomExperience.apiModel.PersonRol;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class UserDetailsCustom implements UserDetails {

  private final Person person;

  public UserDetailsCustom(Person person) {
    this.person = person;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    List<String> roles = new ArrayList<>();
    switch (person.getPersonRol().getRolName()) {
      case "MANAGER":
        roles.add(String.format("ROLE_").concat(PersonRol.ROLE_MANAGER.getRolName()));
      case "RECEPTIONIST":
        roles.add(String.format("ROLE_").concat(PersonRol.ROLE_RECEPTIONIST.getRolName()));
      case "CLIENT":
        roles.add(String.format("ROLE_").concat(PersonRol.ROLE_CLIENT.getRolName()));
        break;
    }
    List<SimpleGrantedAuthority> authorities = new ArrayList<>();
    for (String role : roles) {
      authorities.add(new SimpleGrantedAuthority(role));
    }

    return authorities;
  }

  @Override
  public String getPassword() {
    return person.getPassword();
  }

  @Override
  public String getUsername() {
    return person.getUserName();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return person.isEnabled();
  }

}
