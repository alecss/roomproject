package com.example.roomExperience.connection;

import com.example.roomExperience.apiModel.Book;
import com.example.roomExperience.apiModel.Person;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface BookApi {

  @GET("book")
  Call<List<Book>> get();

  @POST("book")
  Call<Book> insert(@Body Book book);

  @GET("book/{id}")
  Call<Book> findById(@Path("id") Long id);

  @GET("book/{userName}")
  Call<Book> findByUserName(String userName);

  @PUT("book/{id}")
  Call<Book> update(@Path("id") Long id, @Body Book person);

  @DELETE("book/{id}")
  void delete(@Path("id") Long id);

}
