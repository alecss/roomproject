package com.example.roomExperience.connection;

import com.example.roomExperience.apiModel.PersonRol;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PersonRolApi {

  @GET("personRol")
  Call<List<PersonRol>> get();

  @POST("personRol")
  Call<PersonRol> insert(@Body PersonRol personRol);

  @GET("personRol/{id}")
  Call<PersonRol> findById(@Path("id") Long id);

  @GET("personRol/{userName}")
  Call<PersonRol> findByUserName(String userName);

  @PUT("personRol/{id}")
  Call<PersonRol> update(@Path("id") Long id, @Body PersonRol personRol);

  @DELETE("personRol/{id}")
  void delete(@Path("id") Long id);

}
