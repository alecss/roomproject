package com.example.roomExperience.connection;

import com.example.roomExperience.apiModel.RoomType;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RoomTypeApi {

  @GET("RoomType")
  Call<List<RoomType>> get();

  @POST("RoomType")
  Call<RoomType> insert(@Body RoomType object);

  @GET("RoomType/{id}")
  Call<RoomType> findById(@Path("id") Long id);

  @GET("RoomType/{userName}")
  Call<RoomType> findByUserName(String userName);

  @PUT("RoomType/{id}")
  Call<RoomType> update(@Path("id") Long id, @Body RoomType object);

  @DELETE("RoomType/{id}")
  void delete(@Path("id") Long id);

}
