package com.example.roomExperience.connection;

import com.example.roomExperience.apiModel.Person;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PersonApi {

  @GET("person")
  Call<List<Person>> get();

  @POST("person")
  Call<Person> insert(@Body Person person);

  @GET("person/{id}")
  Call<Person> findById(@Path("id") Long id);

  @GET("person/userName/{userName}")
  Call<Person> findByUserName(@Path("userName") String userName);

  @PUT("person/{id}")
  Call<Person> update(@Path("id") Long id, @Body Person person);

  @DELETE("person/{id}")
  void delete(@Path("id") Long id);

}
