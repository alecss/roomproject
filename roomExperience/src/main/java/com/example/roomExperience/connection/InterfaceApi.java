package com.example.roomExperience.connection;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface InterfaceApi<T> {

  @GET("entity")
  Call<List<T>> get();

  @POST("entity")
  Call<T> insert(@Body T object);

  @GET("entity/{id}")
  Call<T> findById(@Path("id") Long id);

  @GET("entity/{userName}")
  Call<T> findByUserName(String userName);

  @PUT("entity/{id}")
  Call<T> update(@Path("id") Long id, @Body T object);

  @DELETE("entity/{id}")
  void delete(@Path("id") Long id);
}
