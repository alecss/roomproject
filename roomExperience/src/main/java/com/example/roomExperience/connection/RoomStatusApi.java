package com.example.roomExperience.connection;

import com.example.roomExperience.apiModel.RoomStatus;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RoomStatusApi {

  @GET("roomStatus")
  Call<List<RoomStatus>> get();

  @POST("roomStatus")
  Call<RoomStatus> insert(@Body RoomStatus object);

  @GET("roomStatus/{id}")
  Call<RoomStatus> findById(@Path("id") Long id);

  @GET("roomStatus/status/{statusName}")
  Call<RoomStatus> findByName(@Path("statusName") String statusName);

  @PUT("roomStatus/{id}")
  Call<RoomStatus> update(@Path("id") Long id, @Body RoomStatus object);

  @DELETE("roomStatus/{id}")
  void delete(@Path("id") Long id);

}
