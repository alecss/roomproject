package com.example.roomExperience.connection.config;


import com.example.roomExperience.connection.BookApi;
import com.example.roomExperience.connection.PersonApi;
import com.example.roomExperience.connection.PersonRolApi;
import com.example.roomExperience.connection.RoomApi;
import com.example.roomExperience.connection.RoomStatusApi;
import com.example.roomExperience.connection.RoomTypeApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

@Configuration
public class RetrofitConfiguration {

  @Bean
  public PersonApi createPersonApi(@Value("${http.api.url}") String baseUrl) {
    return createHttpClient(baseUrl).create(PersonApi.class);
  }

  @Bean
  public PersonRolApi createPersonRolApi(@Value("${http.api.url}") String baseUrl) {
    return createHttpClient(baseUrl).create(PersonRolApi.class);
  }

  @Bean
  public BookApi createBookApi(@Value("${http.api.url}") String baseUrl) {
    return createHttpClient(baseUrl).create(BookApi.class);
  }

  @Bean
  public RoomStatusApi createBookStatusApi(@Value("${http.api.url}") String baseUrl) {
    return createHttpClient(baseUrl).create(RoomStatusApi.class);
  }

  @Bean
  public RoomApi createRoomApi(@Value("${http.api.url}") String baseUrl) {
    return createHttpClient(baseUrl).create(RoomApi.class);
  }

  @Bean
  public RoomTypeApi createRoomTypeApi(@Value("${http.api.url}") String baseUrl) {
    return createHttpClient(baseUrl).create(RoomTypeApi.class);
  }

  private Retrofit createHttpClient(String baseUrl) {
    return new Retrofit.Builder()
        .baseUrl(baseUrl)
        .addConverterFactory(JacksonConverterFactory.create())
        .build();
  }
}
