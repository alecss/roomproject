package com.example.roomExperience.connection;

import com.example.roomExperience.apiModel.Room;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface RoomApi {

  @GET("room")
  Call<List<Room>> get();

  @POST("room")
  Call<Room> insert(@Body Room object);

  @GET("room/{id}")
  Call<Room> findById(@Path("id") Long id);

  @GET("room/name/{name}")
  Call<Room> findByName(String name);

  @PUT("room/{id}")
  Call<Room> update(@Path("id") Long id, @Body Room object);

  @DELETE("room/{id}")
  void delete(@Path("id") Long id);

  @GET("room/free/{id}")
  Call<Room> findByFreeRoomId(@Path("id") Long roomId);

  @GET("room/free")
  Call<List<Room>> findByFree();

  @GET("room/type/{roomType}")
  Call<Iterable<Room>> findRoomsAndType(@Path("roomType") String roomType);

  @GET("room/status/{status}")
  Call<Iterable<Room>> findRoomsAndStatus(@Path("status") String status);
}
