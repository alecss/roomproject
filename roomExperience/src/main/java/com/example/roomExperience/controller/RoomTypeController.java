package com.example.roomExperience.controller;

import com.example.roomExperience.apiModel.RoomType;
import com.example.roomExperience.business.RoomTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/roomType")
public class RoomTypeController extends GenericController<RoomType, Long> {

  public RoomTypeController(
      @Autowired RoomTypeService service) {
    super(service);
  }
}
