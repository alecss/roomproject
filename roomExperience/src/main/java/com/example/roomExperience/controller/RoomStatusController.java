package com.example.roomExperience.controller;

import com.example.roomExperience.apiModel.RoomStatus;
import com.example.roomExperience.business.RoomStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/roomStatus")
public class RoomStatusController extends GenericController<RoomStatus, Long> {

  public RoomStatusController(
      @Autowired RoomStatusService service) {
    super(service);
  }
}
