package com.example.roomExperience.controller;

import com.example.roomExperience.apiModel.Book;
import com.example.roomExperience.business.BookService;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.util.Date;
import javax.management.InstanceNotFoundException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/book")
public class BookController extends GenericController<Book, Long> {
  private final BookService service;
  public BookController(
      @Autowired BookService service) {
    super(service);
    this.service = service;
  }

  @RequestMapping(method = RequestMethod.POST, value = "/booking")
  @ApiOperation(value = "Save the booking part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "POST")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded save the entity <li>void</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  @PreAuthorize("hasRol('CLIENT')")
  public Book booking(
      @ApiParam(value = "Entity object to save", example = "{ object }", required = true)
      @RequestBody BookRequest bookRequest) throws Exception {
    return service.booking(bookRequest.getIdPerson(), bookRequest.getIdRoom(),
        bookRequest.getCheckIn(), bookRequest.getCheckOut());
  }

}

@Getter
@Setter
@NoArgsConstructor
class BookRequest{
  private long idPerson;
  private int idRoom;
  @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss", shape = Shape.STRING)
  private Date checkIn;
  @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss", shape = Shape.STRING)
  private Date checkOut;
}
