package com.example.roomExperience.controller;

import com.example.roomExperience.apiModel.Room;
import com.example.roomExperience.apiModel.RoomStatus;
import com.example.roomExperience.business.RoomService;
import com.example.roomExperience.business.RoomStatusService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.util.Optional;
import javax.management.InstanceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/room")
public class RoomController extends GenericController<Room, Long> {

  private final RoomService service;

  @Autowired
  private RoomStatusService roomStatusService;

  public RoomController(
      @Autowired RoomService service) {
    super(service);
    this.service = service;
  }

  @RequestMapping("/free")
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public Iterable<Room> searchRoomsFree() throws IOException {
    return service.findAllFree();
  }

  @RequestMapping("/status/{status}")
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public Iterable<Room> searchRoomStatus(
      @ApiParam(value = "Entity id search", example = "1", required = true)
      @PathVariable String status) throws IOException {
    return service.findRoomsAndStatus(status);
  }

  @RequestMapping(value = "/change/{idStatus}", method = RequestMethod.POST)
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "POST")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public Room change(
      @ApiParam(value = "Entity id search", example = "1", required = true)
      @PathVariable int idStatus,
      @RequestBody Room room) throws Exception {
    return service.changeStatus(room.getId(), idStatus).get();
  }

  @RequestMapping("/type/{type}")
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public Iterable<Room> searchRoomType(
      @ApiParam(value = "Entity id search", example = "1", required = true)
      @PathVariable String type) throws IOException {
    return service.findRoomsAndType(type);
  }

}
