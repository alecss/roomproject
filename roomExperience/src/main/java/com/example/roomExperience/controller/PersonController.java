package com.example.roomExperience.controller;

import com.example.roomExperience.apiModel.Person;
import com.example.roomExperience.business.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/person")
public class PersonController extends GenericController<Person, Long> {

  public PersonController(
      @Autowired PersonService service) {
    super(service);
  }
}
