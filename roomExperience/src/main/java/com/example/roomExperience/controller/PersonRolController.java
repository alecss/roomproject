package com.example.roomExperience.controller;

import com.example.roomExperience.apiModel.PersonRol;
import com.example.roomExperience.business.PersonRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/personRol")
public class PersonRolController extends GenericController<PersonRol, Long> {

  public PersonRolController(
      @Autowired PersonRolService service) {
    super(service);
  }
}
