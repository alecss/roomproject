package com.example.roomBusiness.service;

import com.example.roomBusiness.entity.Room;
import com.example.roomBusiness.repository.RoomRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RoomService extends GenericService<Room, Long>{

  private final RoomRepository repository;

  public RoomService(
      @Autowired RoomRepository repository) {
    super(repository);
    this.repository = repository;
  }

  public Optional<Room> findByRoomName(String name){
    return repository.findByRoomName(name);
  }

  public Iterable<Room> findRoomsAndStatusFree(){
    return repository.findRoomsAndStatusFree();
  }

  public Optional<Room> findRoomAndStatusFree(long idRoom){
    return repository.findRoomAndStatusFree(idRoom);
  }

  public Iterable<Room> findRoomsAndType(String roomType){
    return repository.findRoomsAndType(roomType);
  }

  public Iterable<Room> findRoomsAndStatus(String status){
    return repository.findRoomsAndStatus(status);
  }

}
