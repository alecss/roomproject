package com.example.roomBusiness.service;

import com.example.roomBusiness.entity.Book;
import com.example.roomBusiness.repository.BookRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class BookService extends GenericService<Book, Long>{

  private final BookRepository repository;

  private static final String FREE = "FREE";
  private static final String BUSY = "BUSY";
  private static final String RESERVED = "RESERVED";
  private static final String MAINTENANCE = "MAINTENANCE";
  private static final String CLEANING = "CLEANING";

  @Autowired
  private RoomStatusService bookStatusService;

  public BookService(
      @Autowired BookRepository repository) {
    super(repository);
    this.repository = repository;
  }

}
