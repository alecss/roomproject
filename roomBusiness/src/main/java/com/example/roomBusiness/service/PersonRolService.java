package com.example.roomBusiness.service;

import com.example.roomBusiness.entity.PersonRol;
import com.example.roomBusiness.repository.PersonRolRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PersonRolService extends GenericService<PersonRol, Long> {

  public PersonRolService(
      @Autowired PersonRolRepository repository) {
    super(repository);
  }
}
