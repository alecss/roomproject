package com.example.roomBusiness.service;

import com.example.roomBusiness.entity.RoomType;
import com.example.roomBusiness.repository.RoomTypeRepository;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RoomTypeService extends GenericService<RoomType, Long>{

  private final RoomTypeRepository repository;

  public RoomTypeService(
      @Autowired RoomTypeRepository repository) {
    super(repository);
    this.repository = repository;
  }
}
