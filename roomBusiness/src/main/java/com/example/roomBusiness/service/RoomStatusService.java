package com.example.roomBusiness.service;

import com.example.roomBusiness.entity.RoomStatus;
import com.example.roomBusiness.repository.RoomStatusRepository;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class RoomStatusService extends GenericService<RoomStatus, Long>{

  private final RoomStatusRepository repository;

  public RoomStatusService(
      @Autowired RoomStatusRepository repository) {
    super(repository);
    this.repository = repository;
  }

  public Optional<RoomStatus> findByStatusName(String statusName){
    return repository.findByStatusName(statusName);
  }

}
