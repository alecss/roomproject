package com.example.roomBusiness.service;

import com.example.roomBusiness.entity.Person;
import com.example.roomBusiness.repository.PersonRepository;
import java.util.Optional;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class PersonService extends GenericService<Person, Long>{

  BCryptPasswordEncoder passwordEncoder;

  @PostConstruct
  void init(){
    passwordEncoder = new BCryptPasswordEncoder();
  }

  private final PersonRepository repository;

  public PersonService( @Autowired PersonRepository repository) {
    super(repository);
    this.repository = repository;
  }

  public Person insert(Person person) {
    person.setPassword(passwordEncoder.encode(person.getPassword()));
    return repository.save(person);
  }

  public Optional<Person> findByUserName(String userName){
    return repository.findByUserName(userName);
  }
}
