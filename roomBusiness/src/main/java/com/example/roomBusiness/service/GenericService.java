package com.example.roomBusiness.service;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

public class GenericService<T, ID> {

  private final CrudRepository<T, ID> repository;

  public GenericService(CrudRepository<T, ID> repository) {
    this.repository = repository;
  }

  CrudRepository<T, ID> getRepository() {
    return this.repository;
  }

  public T insert(T object) {
    return getRepository().save(object);
  }

  public Optional<T> findById(ID id) {
    return getRepository().findById(id);
  }

  public Iterable<T> findAll() {
    return getRepository().findAll();
  }

  public T update(T object) {
    return getRepository().save(object);
  }

  public T delete(ID id) {
    T r = null;
    try {
      getRepository().delete(r = findById(id).orElseThrow(() -> new Exception("Not Found")));
      return r;
    } catch (Exception e) {
      e.printStackTrace();
      return r;
    }
  }

}

