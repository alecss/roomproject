package com.example.roomBusiness.controller;

import com.example.roomBusiness.entity.RoomStatus;
import com.example.roomBusiness.service.RoomStatusService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("roomStatus")
public class RoomStatusController extends GenericController<RoomStatus, Long>{
  private final RoomStatusService service;
  public RoomStatusController(
      @Autowired RoomStatusService service) {
    super(service);
    this.service = service;
  }

  @RequestMapping("/status/{statusName}")
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public RoomStatus search(
      @ApiParam(value = "Entity id search", example = "1", required = true)
      @PathVariable String statusName) throws NotFoundException {
    return service.findByStatusName(statusName)
        .orElseThrow(NotFoundException::new);
  }

}
