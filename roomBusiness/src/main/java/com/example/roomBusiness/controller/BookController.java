package com.example.roomBusiness.controller;

import com.example.roomBusiness.entity.Book;
import com.example.roomBusiness.service.BookService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("book")
public class BookController extends GenericController<Book, Long>{
  private final BookService service;
  public BookController(
      @Autowired BookService service) {
    super(service);
    this.service = service;
  }
  
}
