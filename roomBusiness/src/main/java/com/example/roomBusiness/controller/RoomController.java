package com.example.roomBusiness.controller;

import com.example.roomBusiness.entity.Room;
import com.example.roomBusiness.service.RoomService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("room")
public class RoomController extends GenericController<Room, Long>{
  private final RoomService service;
  public RoomController(
      @Autowired RoomService service) {
    super(service);
    this.service = service;
  }

  @RequestMapping("/name/{name}")
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public Room searchRoomByName(
      @ApiParam(value = "Entity id search", example = "1", required = true)
      @PathVariable String name
  ) throws NotFoundException {
    return service.findByRoomName(name).get();
  }

  @RequestMapping("/free/{id}")
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public Room searchRoomFree(
      @ApiParam(value = "Entity id search", example = "1", required = true)
      @PathVariable long id) throws NotFoundException {
    return service.findRoomAndStatusFree(id).get();
  }

  @RequestMapping("/free")
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public Iterable<Room> searchRoomsFree() throws NotFoundException {
    return service.findRoomsAndStatusFree();
  }

  @RequestMapping("/status/{status}")
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public Iterable<Room> searchRoomStatus(
      @ApiParam(value = "Entity id search", example = "1", required = true)
      @PathVariable String status) throws NotFoundException {
    return service.findRoomsAndStatus(status);
  }

  @RequestMapping("/type/{type}")
  @ApiOperation(value = "Get the entity part of crud controller",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE,
      httpMethod = "GET")
  @ApiResponses({
      @ApiResponse(code = 200, message = "The request has succeeded get entity by id "
          + "<li>Object</li>"),
      @ApiResponse(code = 401, message = "The request requires user authentication"),
      @ApiResponse(code = 403,
          message = "Accessing the resource you were trying to reach is forbidden"),
      @ApiResponse(code = 404,
          message = "The server has not found anything matching the Request-URI"),
      @ApiResponse(code = 400, message = "Request is bad"),
      @ApiResponse(code = 500, message = "Error with backend")
  })
  public Iterable<Room> searchRoomType(
      @ApiParam(value = "Entity id search", example = "1", required = true)
      @PathVariable String type) throws NotFoundException {
    return service.findRoomsAndType(type);
  }

}
