package com.example.roomBusiness.controller;

import com.example.roomBusiness.entity.PersonRol;
import com.example.roomBusiness.service.PersonRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("personRol")
public class PersonRolController extends GenericController<PersonRol, Long>{

  public PersonRolController(
      @Autowired PersonRolService service) {
    super(service);
  }
}
