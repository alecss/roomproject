package com.example.roomBusiness.controller;

import com.example.roomBusiness.entity.RoomType;
import com.example.roomBusiness.service.RoomTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("roomType")
public class RoomTypeController extends GenericController<RoomType, Long>{

  public RoomTypeController(
      @Autowired RoomTypeService service) {
    super(service);
  }
}
