package com.example.roomBusiness.repository;

import com.example.roomBusiness.entity.Book;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "books", path = "book")
public interface BookRepository extends CrudRepository<Book, Long> {

}
