package com.example.roomBusiness.repository;

import com.example.roomBusiness.entity.RoomStatus;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "bookStatus", path = "bookStatus")
public interface RoomStatusRepository extends CrudRepository<RoomStatus, Long> {

  @Query("select b from RoomStatus b where b.statusName = :statusName")
  public Optional<RoomStatus> findByStatusName(String statusName);

}
