package com.example.roomBusiness.repository;

import com.example.roomBusiness.entity.Person;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "persons", path = "restPerson")
public interface PersonRepository extends CrudRepository<Person, Long> {

  @Query("select p from Person p where p.userName = :userName ")
  Optional<Person> findByUserName(String userName);

}
