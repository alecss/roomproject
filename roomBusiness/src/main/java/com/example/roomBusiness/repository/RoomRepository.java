package com.example.roomBusiness.repository;

import com.example.roomBusiness.entity.Book;
import com.example.roomBusiness.entity.Room;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "rooms", path = "room")
public interface RoomRepository extends CrudRepository<Room, Long> {

  @Query("select r from Room r where r.roomName = :roomName")
  public Optional<Room> findByRoomName(String roomName);

  @Query("select r from Room r where r.roomStatus = NULL or r.roomStatus.statusName = 'FREE' ")
  public Iterable<Room> findRoomsAndStatusFree();

  @Query("select r from Room r where r.roomStatus.statusName = :statusName")
  public Iterable<Room> findRoomsAndStatus(String statusName);

  @Query("select r from Room r where r.roomType.roomType = :roomType")
  public Iterable<Room> findRoomsAndType(String roomType);

  @Query("select r from Room r where r.id = :idRoom and (r.roomStatus = NULL or r.roomStatus.statusName = 'FREE') ")
  public Optional<Room> findRoomAndStatusFree(long idRoom);
}
