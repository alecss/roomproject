package com.example.roomBusiness.repository;

import com.example.roomBusiness.entity.PersonRol;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "personRoles", path = "personRol")
public interface PersonRolRepository extends CrudRepository<PersonRol, Long> {
}
