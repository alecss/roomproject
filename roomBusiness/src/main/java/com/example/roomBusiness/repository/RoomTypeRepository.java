package com.example.roomBusiness.repository;

import com.example.roomBusiness.entity.RoomType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "roomTypes", path = "roomType")
public interface RoomTypeRepository extends CrudRepository<RoomType, Long> {
}
