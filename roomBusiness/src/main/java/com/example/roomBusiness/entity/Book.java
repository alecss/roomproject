package com.example.roomBusiness.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import java.util.Date;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Book {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Temporal(TemporalType.TIMESTAMP)
  @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss", shape = Shape.STRING)
  private Date reserve;

  @Temporal(TemporalType.TIMESTAMP)
  @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss", shape = Shape.STRING)
  private Date checkIn;

  @Temporal(TemporalType.TIMESTAMP)
  @JsonFormat(pattern = "yyyy-mm-dd HH:mm:ss", shape = Shape.STRING)
  private Date checkOut;

  @ManyToOne
  @JoinColumn(name = "idPerson")
  private Person person;

  @ManyToOne(cascade = CascadeType.MERGE)
  @JoinColumn(name = "idRoom")
  private Room room;

}
