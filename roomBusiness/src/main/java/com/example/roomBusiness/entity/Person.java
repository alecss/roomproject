package com.example.roomBusiness.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Person {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column(nullable = false, unique = true)
  private String userName;
  private String firstName;
  private String lastName;
  @Column(nullable = false)
  private boolean enabled = false;
  private String password;

  @ManyToOne
  @JoinColumn(name = "idPersonRol")
  private PersonRol personRol;

}
